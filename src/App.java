import java.util.Random;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;

public class App {
    static void copy(String text) {
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }

    static String mock(String text) {
        StringBuilder sb = new StringBuilder();
        Random rand = new Random();

        for (char character : text.toCharArray()) {
            int n = rand.nextInt(10);

            if (n % 2 == 0) {
                sb.append(Character.toUpperCase(character));
            } else {
                sb.append(Character.toLowerCase(character));
            }
        }

        String mockedText = sb.toString();

        copy(mockedText);
        return mockedText;
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println("Error: not enough arguments.");
            System.out.println("Usage: java App <text>");
            System.out.println("Example: java App Hello World");
            System.exit(1);
        }

        String text = "";

        for (int i = 0; i < args.length; i++) {
            text += args[i] + " ";
        }

        System.out.println(mock(text));
    }
}
